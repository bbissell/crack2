#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include "md5.h"

const int PASS_LEN=20;        // Maximum any password will be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    char *guessHash = md5(guess, strlen(guess));
    if (strcmp(hash, guessHash) == 0) {
        return 1;
    } else {
        return 0;
    }
    free(hash);
    free(guessHash);

}

// Read in the dictionary file and return the array of strings
// and store the length of the array in size.
// This function is responsible for opening the dictionary file,
// reading from it, building the data structure, and closing the
// file.
char **read_dictionary(char *filename, int *size)
{
    struct stat info;
    if(stat(filename, &info) == -1) {
        printf("Can't stat the file\n");
        exit(1);
    }
    int fileSize = info.st_size;
    
    printf("File is %d bytes\n", fileSize);
    
    char *content = malloc(fileSize + 1);
    FILE *inputFile = fopen(filename, "r");
    if (inputFile == NULL) {
        printf("Can't open file %s", filename);
        exit(1);
    }
    fread(content, 1, fileSize, inputFile);
    fclose(inputFile);
    content[fileSize] = '\0';
    
    int lineCount = 0;
    for (int i=0; i < fileSize; i++) {
        if (content[i] == '\n') {
            lineCount++;
        }
    }
    
    char **lines = malloc(lineCount * sizeof(char *));
    
    lines[0] = strtok(content, "\n");
    int i=1;
    while ((lines[i] = strtok(NULL, "\n")) != NULL) {
        i++;
    }
    
    *size = lineCount;
    return lines;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // Read the dictionary file into an array of strings.
    int dlen;
    char **dict = read_dictionary(argv[2], &dlen);

    // Open the hash file for reading.
    FILE *hashFile = fopen(argv[1], "r");
    if (hashFile == NULL) {
        printf("Can't open %s for reading\n", argv[1]);
        exit(1);
    }

    // For each hash, try every entry in the dictionary.
    // Print the matching dictionary entry.
    // Need two nested loops.
    char hash[40];
    while (fgets(hash, 40, hashFile) != NULL) {
        hash[strlen(hash) - 1] = '\0';
        
        for (int i=0; i < dlen; i++) {
            if (tryguess(hash, dict[i]) == 1) {
                printf("%s %s\n", hash, dict[i]);
            }
        }
    }
}
